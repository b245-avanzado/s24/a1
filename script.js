let getCube = Math.pow(2, 3);

console.log(`The cube of 2 is ${getCube}.`);

let address = [ "Gen. Luna", "Pagsanjan", "Laguna", "4008" ];

let [ street, municipality, province, zipcode ] = address;

console.log(`I went to ${street}, ${municipality}, ${province}, ${zipcode}.`);

let animal = {
	name: "Perry",
	species: "Platypus",
	measurement: "2 feet"
}

let {name, species, measurement} = animal

console.log(`Hi, my name is ${name}. I am a ${species} standing ${measurement} tall.`);

let numbers = [1, 3, 5, 7, 9];

numbers.forEach(printNum = (num) => console.log(num));

let reduceNumber = numbers.reduce(printSum = (x, y) => x+y)

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;
	}
}

let dog = new Dog("Coco", 1, "Poodle");

console.log(dog);